
<!-- README.md is generated from README.Rmd. Please edit that file -->

# p21themes <a href="https://gitlab.com/dickoa/p21themes"><img src='man/figures/p21themes_sticker.png' align="right" alt="" width="140" /></a>

<!-- badges: start -->

[![Project Status: Active - Initial development is in progress, but
there has not yet been a stable, usable release suitable for the
public.](http://www.repostatus.org/badges/latest/wip.svg)](http://www.repostatus.org/#wip)
[![GitLab CI Build
Status](https://gitlab.com/dickoa/p21themes/badges/master/pipeline.svg)](https://gitlab.com/dickoa/p21themes/pipelines)
[![CRAN
status](https://www.r-pkg.org/badges/version/p21themes)](https://cran.r-project.org/package=p21themes)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
<!-- badges: end -->

## Overview

## Installation

This package is not on yet on CRAN and to install it, you will need the
remotes package.

``` r
install.packages("remotes")
remotes::install_gitlab("dickoa/p21themes")
```

## Content

## Fonts

## Usage

``` r
library(tidyverse)
library(scales)
library(p21themes)
```

### Base ggplot2 theme

``` r
data(idp, package = "p21themes")
idp_total <- idp |>
  group_by(year) |>
  summarise(idp = sum(idp, na.rm = TRUE) / 1e6) |>
  ungroup()
glimpse(idp_total)
#> Rows: 10
#> Columns: 2
#> $ year <int> 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 20…
#> $ idp  <dbl> 10.32914, 11.62813, 10.76541, 13.48312, 16.31171, …
```

``` r
ggplot(idp_total) +
  geom_col(aes(x = year, y = idp),
           width = 0.8) +
  labs(title = "Globalement IDP displacement | 2010 - 2020",
       y = "Number of people (in million)",
       caption = "Source: UNHCR Refugee Data Finder\n© UNHCR, The UN Refugee Agency") +
  scale_y_continuous(expand = expansion(c(0, 0.1))) +
  scale_x_continuous(breaks = pretty_breaks(10)) +
  theme_p21(grid = "Y",
            axis_title = "y")
```

<img src="man/figures/README-plot-theme-1.png" width="2100" />

### Project 21 color palette

All recommended data visualization colors are accessible as **palettes**
or **scales** (color/fill).

``` r
display_p21_all()
```

<img src="man/figures/README-palette-1.png" width="2100" />

### Base theme and color scale

``` r
ggplot(idp_total) +
  geom_col(aes(x = year, y = idp),
           fill = p21_pal(n = 1, "p21_amber"),
           width = 0.8) +
  labs(title = "Globalement IDP displacement | 2010 - 2020",
       y = "Number of people (in million)",
       caption = "Source: UNHCR Refugee Data Finder\n© UNHCR, The UN Refugee Agency") +
  scale_y_continuous(expand = expansion(c(0, 0.1))) +
  scale_x_continuous(breaks = pretty_breaks(10)) +
  theme_p21(grid = "Y",
            axis_title = "y")
```

<img src="man/figures/README-plot-theme-fill-1.png" width="2100" />

``` r
idp_total_sex <- idp |>
  group_by(year, sex) |>
  summarise(idp = sum(idp, na.rm = TRUE) / 1e6) |>
  ungroup()
#> `summarise()` has grouped output by 'year'. You can override
#> using the `.groups` argument.
glimpse(idp_total_sex)
#> Rows: 20
#> Columns: 3
#> $ year <int> 2010, 2010, 2011, 2011, 2012, 2012, 2013, 2013, 20…
#> $ sex  <chr> "female", "male", "female", "male", "female", "mal…
#> $ idp  <dbl> 5.158210, 5.170928, 5.874562, 5.753568, 5.389737, …
```

``` r
ggplot(idp_total_sex) +
  geom_col(aes(x = year, y = idp, fill = sex),
           width = 0.8,
           position = position_dodge(width = 0.9)) +
  scale_fill_p21_d(palette = "p21_main") +
  scale_y_continuous(expand = expansion(c(0, 0.1))) +
  labs(title = "Globalement IDP displacement | 2010 - 2020",
       y = "Number of people (in million)",
       caption = "Source: UNHCR Refugee Data Finder\n© UNHCR, The UN Refugee Agency") +
  scale_x_continuous(breaks = pretty_breaks(10)) +
  theme_p21(grid = "Y",
            axis_title = "y")
```

<img src="man/figures/README-plot-theme-color-1.png" width="2100" />

## Getting help

Please report any issues or bugs on
[GitLab](https://gitlab.com/dickoa/p21themes/issues), try to include a
minimal reproducible example to help us understand.

## Code of Conduct

Please note that the mynewpack project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
