#' Projet 21[ggplot2] theme following brand recommendations
#'
#' \code{theme_p21} provides a basic \bold{P21} theme
#' to use in \bold{ggplot2} commands.
#'
#'
#' @rdname theme_p21
#'
#' @param font_size Base font size
#' @param font_family Base font family, default "NotoSans"
#' @param line_size Base line size for axis. Gridlines are line_size/2
#' @param rel_small Relative size of small text (e.g., axis labels)
#' @param rel_tiny Relative size of tiny text (e.g., caption)
#' @param rel_large Relative size of large text (e.g., title)
#' @param grid Turn on and off the grids. "X" or "Y" for major and "x" or "y" for minor
#' @param axis Turn on and off axis. Use "X" or "Y" to have only the correspondent active
#' @param axis_title Turn on and off axis. Use "X" or "Y" to have only the correspondent active
#' @param axis_text Turn on and off axis. Use "X" or "Y" to have only the correspondent active
#' @param axis_ticks Turn on and off ticks. Use "X" or "Y" to have only the correspondent active
#' @param legend Turn on and off the legend, default TRUE.
#' @param legend_title Turn on and off title of the legend, default FALSE.
#'
#' @return The theme style
#' @import ggplot2
#' @importFrom grid unit
#' @export
#'
#' @examples
#' \dontrun{
#' library(ggplot2)
#'
#' ggplot(mpg, aes(displ, hwy)) +
#' geom_point() +
#' theme_p21()
#' }
theme_p21 <- function(font_size = 9, font_family = "NotoSans", line_size = .5,
                      rel_small = 8 / 9, rel_tiny = 7 / 9, rel_large = 12 / 9,
                      grid = TRUE, axis = "x", axis_text = TRUE, axis_title = TRUE,
                      axis_ticks = FALSE, legend = TRUE, legend_title = FALSE) {
  # TODO Plot left aligned legend
  # TODO Blue line on top
  # TODO Horizontal Y-axis title above axis

  # margin
  half_line <- font_size / 2

  p21_amber <- "#E69F00"
  dark_grey <- "#191919"
  medium_grey <- "#666666"
  light_grey <- "#CCCCCC"

  # work off theme_minimal
  ret <- ggplot2::theme_minimal(base_family = font_family, base_size = font_size)

  # reset all base element
  ret <- ret + ggplot2::theme(line = ggplot2::element_line(
    color = "black", size = line_size, linetype = 1, lineend = "butt"
  ))
  ret <- ret + ggplot2::theme(rect = ggplot2::element_rect(
    fill = NA, color = NA, size = line_size, linetype = 1
  ))
  ret <- ret + ggplot2::theme(text = ggplot2::element_text(
    family = font_family, face = "plain", color = dark_grey,
    size = font_size, hjust = 0.5, vjust = 0.5, angle = 0, lineheight = .9,
    margin = ggplot2::margin(), debug = FALSE
  ))

  # legend
  if (!legend) {
    ret <- ret + theme(legend.position = "none")
  } else {
    ret <- ret + ggplot2::theme(legend.background = ggplot2::element_blank())
    ret <- ret + ggplot2::theme(legend.spacing = grid::unit(font_size, "pt"))
    ret <- ret + ggplot2::theme(legend.margin = ggplot2::margin(0, 0, 0, 0))
    ret <- ret + ggplot2::theme(legend.key = ggplot2::element_blank())
    ret <- ret + ggplot2::theme(legend.key.size = grid::unit(1.2 * font_size, "pt"))
    ret <- ret + ggplot2::theme(legend.text = ggplot2::element_text(color = dark_grey, size = rel(rel_small)))
    ret <- ret + ggplot2::theme(legend.position = "top")
    ret <- ret + ggplot2::theme(legend.direction = "horizontal")
    ret <- ret + ggplot2::theme(legend.justification = 0)
    ret <- ret + ggplot2::theme(legend.box.margin = ggplot2::margin(0, 0, 0, 0))
    ret <- ret + ggplot2::theme(legend.box.background = ggplot2::element_blank())
    ret <- ret + ggplot2::theme(legend.box.spacing = grid::unit(font_size, "pt"))
    if (!legend_title) {
      ret <- ret + theme(legend.title = element_blank())
    } else {
      ret <- ret + ggplot2::theme(legend.title = ggplot2::element_text(size = rel(rel_small),
                                                                       color = dark_grey,
                                                                       hjust = 0))
    }
  }

  # grid
  if (inherits(grid, "character") | grid == TRUE) {
    ret <- ret + ggplot2::theme(panel.grid = ggplot2::element_line(color = light_grey, size = line_size / 2))
    ret <- ret + ggplot2::theme(panel.grid.major = ggplot2::element_line(color = light_grey, size = line_size / 2))
    ret <- ret + ggplot2::theme(panel.grid.minor = ggplot2::element_line(color = light_grey, size = line_size / 2))

    if (inherits(grid, "character")) {
      if (regexpr("X", grid)[1] < 0) ret <- ret + ggplot2::theme(panel.grid.major.x = ggplot2::element_blank())
      if (regexpr("Y", grid)[1] < 0) ret <- ret + ggplot2::theme(panel.grid.major.y = ggplot2::element_blank())
      if (regexpr("x", grid)[1] < 0) ret <- ret + ggplot2::theme(panel.grid.minor.x = ggplot2::element_blank())
      if (regexpr("y", grid)[1] < 0) ret <- ret + ggplot2::theme(panel.grid.minor.y = ggplot2::element_blank())
    }
  } else {
    ret <- ret + ggplot2::theme(panel.grid = ggplot2::element_blank())
  }

  # panel
  ret <- ret + ggplot2::theme(panel.background = ggplot2::element_blank())
  ret <- ret + ggplot2::theme(panel.border = ggplot2::element_blank())
  ret <- ret + ggplot2::theme(panel.spacing = grid::unit(half_line, "pt"))
  ret <- ret + ggplot2::theme(panel.ontop = FALSE)

  # axis line
  if (inherits(axis, "character") | axis == TRUE) {
    ret <- ret + ggplot2::theme(axis.line = ggplot2::element_line(
      color = dark_grey, size = line_size,
      lineend = "square"
    ))
    if (inherits(axis, "character")) {
      axis <- tolower(axis)
      if (regexpr("x", axis)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.line.x = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.line.x = ggplot2::element_line(
          color = dark_grey, size = line_size,
          lineend = "square"
        ))
      }
      if (regexpr("y", axis)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.line.y = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.line.y = ggplot2::element_line(
          color = dark_grey, size = line_size,
          lineend = "square"
        ))
      }
    } else {
      ret <- ret + ggplot2::theme(axis.line.x = ggplot2::element_line(
        color = dark_grey, size = line_size,
        lineend = "square"
      ))
      ret <- ret + ggplot2::theme(axis.line.y = ggplot2::element_line(
        color = dark_grey, size = line_size,
        lineend = "square"
      ))
    }
  } else {
    ret <- ret + ggplot2::theme(axis.line = ggplot2::element_blank())
  }

  # axis text
  if (inherits(axis_text, "character") | axis_text == TRUE) {
    ret <- ret + ggplot2::theme(axis.text = ggplot2::element_text(size = rel(rel_small), color = dark_grey))
    if (inherits(axis_text, "character")) {
      axis_text <- tolower(axis_text)
      if (regexpr("x", axis_text)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.text.x = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.text.x = ggplot2::element_text(margin = ggplot2::margin(
          t = rel_small * font_size / 4
        ), vjust = 1))
        ret <- ret + ggplot2::theme(axis.text.x.top = ggplot2::element_text(margin = ggplot2::margin(
          b = rel_small * font_size / 4
        ), vjust = 0))
      }
      if (regexpr("y", axis_text)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.text.y = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.text.y = ggplot2::element_text(margin = ggplot2::margin(
          r = rel_small * font_size / 4
        ), hjust = 1))
        ret <- ret + ggplot2::theme(axis.text.y.right = ggplot2::element_text(margin = ggplot2::margin(
          l = rel_small * font_size / 4
        ), hjust = 0))
      }
    } else {
      ret <- ret + ggplot2::theme(axis.text.x = ggplot2::element_text(margin = ggplot2::margin(
        t = rel_small * font_size / 4
      ), vjust = 1))
      ret <- ret + ggplot2::theme(axis.text.x.top = ggplot2::element_text(margin = ggplot2::margin(
        b = rel_small * font_size / 4
      ), vjust = 0))
      ret <- ret + ggplot2::theme(axis.text.y = ggplot2::element_text(margin = ggplot2::margin(
        r = rel_small * font_size / 4
      ), hjust = 1))
      ret <- ret + ggplot2::theme(axis.text.y.right = ggplot2::element_text(margin = ggplot2::margin(
        l = rel_small * font_size / 4
      ), hjust = 0))
    }
  } else {
    ret <- ret + ggplot2::theme(axis.text = ggplot2::element_blank())
  }

  # axis title
  if (inherits(axis_title, "character") | axis_title == TRUE) {
    ret <- ret + ggplot2::theme(axis.title = ggplot2::element_text(size = rel(rel_small), color = medium_grey))
    if (inherits(axis_title, "character")) {
      axis_title <- tolower(axis_title)
      if (regexpr("x", axis_title)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.title.x = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.title.x = ggplot2::element_text(margin = ggplot2::margin(
          t = rel_small * font_size / 4
      ), vjust = 1))
        ret <- ret + ggplot2::theme(axis.title.x.top = ggplot2::element_text(margin = ggplot2::margin(
          b = rel_small * font_size / 4
        ), vjust = 0))
      }
      if (regexpr("y", axis_title)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.title.y = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.title.y = ggplot2::element_text(angle = 90, margin = ggplot2::margin(
          r = rel_small * font_size / 4
        ), vjust = 1))
        ret <- ret + ggplot2::theme(axis.title.y.right = ggplot2::element_text(angle = -90, margin = ggplot2::margin(
          l = rel_small * font_size / 4
        ), vjust = 0))
      }
    } else {
      ret <- ret + ggplot2::theme(axis.title.x = ggplot2::element_text(margin = ggplot2::margin(
        t = rel_small * font_size / 4
      ), vjust = 1))
      ret <- ret + ggplot2::theme(axis.title.x.top = ggplot2::element_text(margin = ggplot2::margin(
        b = rel_small * font_size / 4
      ), vjust = 0))
      ret <- ret + ggplot2::theme(axis.title.y = ggplot2::element_text(angle = 90, margin = ggplot2::margin(
        r = rel_small * font_size / 4
      ), vjust = 1))
      ret <- ret + ggplot2::theme(axis.title.y.right = ggplot2::element_text(angle = -90, margin = ggplot2::margin(
        l = rel_small * font_size / 4
      ), vjust = 0))
    }
  } else {
    ret <- ret + ggplot2::theme(axis.title = ggplot2::element_blank())
  }

  # ticks
  if (inherits(axis_ticks, "character") | axis_ticks == TRUE) {
    ret <- ret + ggplot2::theme(axis.ticks.length = grid::unit(half_line / 2, "pt"))
    ret <- ret + ggplot2::theme(axis.ticks = ggplot2::element_line(color = dark_grey, size = line_size / 2))
    if (inherits(axis_ticks, "character")) {
      axis_ticks <- tolower(axis_ticks)
      if (regexpr("x", axis_ticks)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.ticks.x = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.ticks.x = ggplot2::element_line(color = dark_grey, size = line_size / 2))
      }
      if (regexpr("y", axis_ticks)[1] < 0) {
        ret <- ret + ggplot2::theme(axis.ticks.y = ggplot2::element_blank())
      } else {
        ret <- ret + ggplot2::theme(axis.ticks.y = ggplot2::element_line(color = dark_grey, size = line_size / 2))
      }
    } else {
      ret <- ret + ggplot2::theme(axis.ticks.x = ggplot2::element_line(color = dark_grey, size = line_size / 2))
      ret <- ret + ggplot2::theme(axis.ticks.y = ggplot2::element_line(color = dark_grey, size = line_size / 2))
    }
  } else {
    ret <- ret + ggplot2::theme(axis.ticks = element_blank())
  }

  # strip
  ret <- ret + ggplot2::theme(strip.text = ggplot2::element_text(
    hjust = 0, size = font_size,
    margin = ggplot2::margin(half_line / 2, half_line / 2, half_line / 2, half_line / 2)
  ))

  # title and subtitle
  ret <- ret + ggplot2::theme(plot.title = ggplot2::element_text(
    size = rel(rel_large), color = "black", face = "bold",
    hjust = 0, vjust = 1,
    margin = ggplot2::margin(b = font_size),
  ))
  ret <- ret + ggplot2::theme(plot.subtitle = ggplot2::element_text(
    size = font_size, color = dark_grey, face = "plain",
    hjust = 0, vjust = 1,
    margin = ggplot2::margin(t = -half_line, b = font_size * rel_large)
  ))
  ret <- ret + ggplot2::theme(plot.title.position = "plot")

  # caption
  ret <- ret + ggplot2::theme(plot.caption = ggplot2::element_text(
    size = rel(rel_tiny), color = medium_grey,
    hjust = 0, vjust = 1,
    margin = ggplot2::margin(t = half_line)
  ))
  ret <- ret + ggplot2::theme(plot.caption.position = "plot")

  # tag
  ret <- ret + ggplot2::theme(plot.tag = ggplot2::element_text(
    size = font_size, color = p21_amber,
    hjust = 0, vjust = 1
  ))
  ret <- ret + ggplot2::theme(plot.tag.position = c(0, 1))

  # plot
  ret <- ret + ggplot2::theme(plot.background = ggplot2::element_blank())
  ret <- ret + ggplot2::theme(plot.margin = ggplot2::margin(font_size, font_size, font_size, font_size))

  # class for condition on tag
  class(ret) <- c("conditional_p21_theme", class(ret))
  attr(ret, "font_size") <- font_size
  ret
}

#' @importFrom ggplot2 ggplot_add element_text margin
#' @noRd
#' @export
ggplot_add.conditional_p21_theme <- function(object, plot, object_name) {
  # TODO Find a way to get margin based on font_size from the main theme.
  # It's called twice to have the font_size (fixed it after)
  plot <- add_gg_theme(plot, object)
  font_size <- plot$theme$text$size
  if (!is.null(plot$labels$tag)) {
    object <- object +
      ggplot2::theme(plot.title = ggplot2::element_text(
        margin = ggplot2::margin(t = font_size * 1.5, b = font_size)))
    plot <- add_gg_theme(plot, object)
  }
  plot
}

#' Update matching font defaults for text geoms
#'
#' Updates [ggplot2::geom_label] and [ggplot2::geom_text] font defaults
#'
#' @param family Font family, default "NotoSans"
#' @param face Font face, default "plain"
#' @param size Size
#' @param color Font color, default "#191919"
#'
#' @export
update_geom_font_defaults <- function(family = "NotoSans",
                                      face = "plain",
                                      size = 3.5,
                                      color = "#191919") {
  ggplot2::update_geom_defaults("text",
                                list(family = family, face = face,
                                     size = size, color = color))
  ggplot2::update_geom_defaults("label",
                                list(family = family, face = face,
                                     size = size, color = color))
}
